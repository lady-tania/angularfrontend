import { Component, OnInit } from '@angular/core';
import { Operador } from '../../../interfaces/operador';
import { OperadoresService } from '../../../services/operadores.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  operador: Operador = {
    ci: null,
    paterno: null,
    materno: null,
    nombres: null,
    fecha_nacimiento: null,
    telefono: null
  };
  id: any;
  editing: boolean = false;
  operadores: Operador[];
  title: string = 'Nuevo Operador';
  public isError = false;
  constructor(private operadoresService: OperadoresService,
              private activateRoute: ActivatedRoute,
              private router:Router) {
    this.id = this.activateRoute.snapshot.params['id'];
    
    if(this.id) {
      this.title = 'Editando Operador';
      this.editing = true;
      this.operadoresService.get().subscribe((data: Operador[])=>{
        this.operadores = data;
        this.operador = this.operadores.find((d)=> { return d.id == this.id});
        console.log(this.operador);
      }, (error)=>{
        console.log(error);
      });
    } else {
      this.editing = false;
    }
  }

  ngOnInit(): void {
  }
  saveOperator() {
    if(this.editing){      
      this.operadoresService.put(this.operador).subscribe((data) => {
        alert('Operador modificado');
        console.log(data);
      }, (error)=> {
        console.log(error);
        alert('Ocurrio un error');
      });

    } else {
      this.operadoresService.save(this.operador).subscribe((data) => {
        alert('Operador guardado');
        console.log(data);
      }, (error)=> {
        console.log(error);
        alert('Ocurrio un error');
      });
    }
    this.router.navigate(['/operadores']);
  }

}
