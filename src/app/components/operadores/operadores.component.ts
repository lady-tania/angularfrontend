import { Component, OnInit } from '@angular/core';
import { OperadoresService } from '../../services/operadores.service';
import {Operador } from '../../interfaces/operador';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-operadores',
  templateUrl: './operadores.component.html',
  styleUrls: ['./operadores.component.css']
})
export class OperadoresComponent implements OnInit {
  operadores: Operador[];

  constructor(private operadoresService: OperadoresService, private httpClient: HttpClient) { 
    this.getOperadores();
  }

  getOperadores(){
    this.operadoresService.get().subscribe((data: Operador[])=>{
      this.operadores = data;
    }, (error)=>{
      console.log(error);
      alert('Ocurrio un error');
    })
  }

  ngOnInit(): void {
  }
  delete(id) {
    if (confirm('Seguro que quieres eliminar al operador?')){
      this.operadoresService.delete(id).subscribe((data)=>{
        alert('Eliminado con exito');
        this.getOperadores();
        console.log(data);
      }, (error)=>{
        console.log(error);
        alert('Ocurrio un error');
      })
    }
  }

}
