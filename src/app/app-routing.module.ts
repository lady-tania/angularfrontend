import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { OperadoresComponent } from './components/operadores/operadores.component';
import { FormComponent } from './components/operadores/form/form.component';

const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'home', component: HomeComponent},
  {path:'operadores', component: OperadoresComponent},
  {path:'form', component: FormComponent},
  {path:'form/:id', component: FormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }




