import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { OperadoresComponent } from './components/operadores/operadores.component';
import { FormComponent } from './components/operadores/form/form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule} from '@angular/forms';
import { FormValidationDirective } from './validations/form-validation.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OperadoresComponent,
    FormComponent,
    FormValidationDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
