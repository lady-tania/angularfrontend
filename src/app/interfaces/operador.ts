export interface Operador {
    id?: number;
    ci: string;
    paterno: string;
    materno?: string;
    nombres: string;
    fecha_nacimiento: string;
    telefono?: string;
    created_at?: string;
    update_at?: string;
}